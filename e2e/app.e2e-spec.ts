import { SiteVisitorPage } from './app.po';

describe('site-visitor App', () => {
  let page: SiteVisitorPage;

  beforeEach(() => {
    page = new SiteVisitorPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
