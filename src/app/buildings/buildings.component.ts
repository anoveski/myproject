import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {PropertyCard} from '../models/property-card-model/property-card';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.css']
})
export class BuildingsComponent implements OnInit {

  public apartments: PropertyCard[] = [];

  constructor(private httpClient: HttpClient,
              private titleService: Title) {

    this.titleService.setTitle('Apartments - ' + environment.appName);
  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({
        'PropertyType': 'Apartment'
      })
    };
    this.httpClient.get('https://my-server.azurewebsites.net/api/get-properties-by-type', httpOptions)
      .subscribe((data: PropertyCard[]) => {
        this.apartments = data;
      })
  }
}
