import {RouterModule, Routes} from '@angular/router';
import {BuildingsComponent} from './buildings.component';

export const buildingsRoutes: Routes = [
  {
    path: 'apartments', component: BuildingsComponent
  }
];
export const BuildingRouterModule = RouterModule.forChild(buildingsRoutes);
