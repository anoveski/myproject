import {RouterModule, Routes} from '@angular/router';
import {HousesComponent} from './houses.component';

export const housesRoutes: Routes = [
  {
    path: 'family-houses', component: HousesComponent
  }
];
export const HousesRouterModule = RouterModule.forChild(housesRoutes);
