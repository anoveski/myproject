import {Component, OnInit} from '@angular/core';
import {PropertyCard} from '../models/property-card-model/property-card';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent implements OnInit {

  public houses: PropertyCard[] = [];

  constructor(private httpClient: HttpClient,
              private titleService: Title) {

    this.titleService.setTitle('Family Houses - ' + environment.appName);
  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({
        'PropertyType': 'Multi-family house',
      })
    };
    this.httpClient.get('https://my-server.azurewebsites.net/api/get-properties-by-type', httpOptions)
      .subscribe((data: PropertyCard[]) => {
        this.houses = data;
      })
  }
}
