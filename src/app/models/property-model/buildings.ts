import {PropertyUnit} from './propertyUnit';
import {Seller} from './seller';
import {StatusOfTheParts} from './statusOfTheParts';

export interface Buildings {
  _id: string;
  title: string;
  price: number;
  rating: number;
  propertyUnit: PropertyUnit
  details: string;
  profilePicture: string;
  seller: Seller;
  advertisedDate: string;
  buildingGallery: Array<any>;
  statusOfTheParts: StatusOfTheParts
}
