export interface Seller {
  sellerId: string;
  sellerName: string;
  sellerSurname: string;
  phoneNumber: string;
  seller_email: string;
}
