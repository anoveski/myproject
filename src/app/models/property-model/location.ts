import {GpsCoordinates} from './gpsCoordinates';

export interface Location {
  addressLine: string;
  addressLine2: string;
  zipCode: number;
  place: string;
  province: string;
  country: string;
  countryIsoCode: string;
  gpsCoordinates: GpsCoordinates;
}
