export interface GpsCoordinates {
  latitude: number;
  longitude: number;
}
