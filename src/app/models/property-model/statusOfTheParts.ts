export interface StatusOfTheParts {
  bathroomsRating: number,
  toiletsRating: number,
  kitchenRating: number,
  roomsRating: number,
  corridorsRating: number,
  roofRating: number
  windowsRating: number;
}
