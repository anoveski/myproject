export interface Interior {
  numberOfFloors: number;
  numberOfBathrooms: number;
  numberOfToilets: number;
  numberOfKitchens: number;
  numberOfRooms: number;
  numberCorridors: number;
}
