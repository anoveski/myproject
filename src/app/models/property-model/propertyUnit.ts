import {Location} from './location';
import {Interior} from './interior';
import {Area} from './area';

export interface PropertyUnit {
  typeOfBuilding: string;
  yearOfConstruction: number;
  area: Area;
  interior: Interior;
  location: Location;
}
