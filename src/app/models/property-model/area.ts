export interface Area {
  baseSize: number;
  yardStatus: string;
  yardSize: number;
}
