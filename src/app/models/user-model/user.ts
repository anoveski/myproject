export interface User {
  id: string;
  first_name: string;
  last_name: string
  user_gender: string;
  date_of_birth: string;
  phone_number: number;
  user_email: string;
}
