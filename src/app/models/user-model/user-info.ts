import {User} from './user';

export interface UserInfo {
  status: boolean;
  user: User
}
