export interface PropertyCard {
  id: string;
  title: string;
  price: number;
  rating: number;
  typeOfBuilding: string;
  buildingProfilePicture: string;
  province: string;
  place: string;
  country: string;
}
