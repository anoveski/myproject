import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from './register.component';

export const registerRoutes: Routes = [
  {
    path: 'register', component: RegisterComponent
  }
];
export const RegisterRouterModule = RouterModule.forChild(registerRoutes);
