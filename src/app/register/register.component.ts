import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatDialog} from '@angular/material';
import {LoadingDialogComponent} from '../loading-dialog/loading-dialog.component';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],

})

export class RegisterComponent implements OnInit {

  first_name: string;
  last_name: string;
  selectedGender: string;
  date_of_birth: string;
  phone_number: number;
  user_email: string;
  password: string;
  success: boolean;
  env = environment;

  loadingDialog: any;

  genders = [
    {value: 'male-0', viewValue: 'Male'},
    {value: 'female-1', viewValue: 'Female'},
  ];

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  minDate = new Date(1900, 0, 1);
  maxDate = new Date(Date.now());

  constructor(private datePipe: DatePipe,
              private httpClient: HttpClient,
              private loading: MatDialog,
              public login: MatDialog,
              public titleService: Title) {
    this.titleService.setTitle('Register - ' + environment.appName);

    this.first_name = '';
    this.last_name = '';
    this.selectedGender = '';
    this.date_of_birth = '';
    this.phone_number = null;
    this.user_email = '';
    this.password = '';
    this.success = false;
  }

  ngOnInit() {
  }

  registerUser() {

    if (this.first_name && this.first_name.length >= 3 && this.last_name && this.last_name && this.last_name.length >= 3 &&
      this.selectedGender && this.selectedGender.length >= 3 && this.phone_number >= 10000 &&
      this.user_email && this.user_email.length >= 12 && this.password && this.password.length >= 5) {

      this.loadingDialog = this.loading.open(LoadingDialogComponent, {
        width: '148px',
        height: '181px',
        data: {status: 'Registering'}
      });

      const user = {
        first_name: this.first_name,
        last_name: this.last_name,
        user_gender: this.selectedGender,
        date_of_birth: this.datePipe.transform(this.date_of_birth, 'MM/dd/yyyy'),
        phone_number: this.phone_number,
        user_email: this.user_email,
        password: this.password
      };
      const httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
      };

      this.httpClient.post('https://my-server.azurewebsites.net/api/register', user, httpOptions)
        .subscribe(
          (data: any) => {
            if (data.success === false) {
              this.loadingDialog.close();
              alert(data.message)
            } else {
              this.loadingDialog.close();
              alert(data.message);
              this.first_name = '';
              this.last_name = '';
              this.selectedGender = '';
              this.date_of_birth = '';
              this.phone_number = null;
              this.user_email = '';
              this.password = '';
            }
          }
        );
    } else {
      alert('Enter valid credentials');
    }
  }
}
