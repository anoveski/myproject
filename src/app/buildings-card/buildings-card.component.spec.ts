import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingsCardComponent } from './buildings-card.component';

describe('BuildingsCardComponent', () => {
  let component: BuildingsCardComponent;
  let fixture: ComponentFixture<BuildingsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
