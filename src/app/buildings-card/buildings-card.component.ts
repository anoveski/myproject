import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PropertyCard} from '../models/property-card-model/property-card';

@Component({
  selector: 'app-buildings-card',
  templateUrl: './buildings-card.component.html',
  styleUrls: ['./buildings-card.component.css']
})
export class BuildingsCardComponent implements OnInit {

  @Input() buildingByType: PropertyCard;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public viewDetails(): void {
    this.router.navigate(['buildings/details/', this.buildingByType.id]);
  }
}
