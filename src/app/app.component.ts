import {Component} from '@angular/core';
import {LoginDialogComponent} from './login-dialog/login-dialog.component';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {AuthService} from './services/auth-service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private user_email: string;
  private password: string;

  constructor(public login: MatDialog,
              public router: Router,
              public auth: AuthService) {
  }


  public loginDialog(): void {
    const dialogRef = this.login.open(LoginDialogComponent, {
      width: '400px',
      data: {user_email: this.user_email, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.user_email = result;
      this.password = result;
    });
  }

  public logout(): void {
    localStorage.removeItem('token');
    this.auth.loggedIn();
    this.router.navigate(['home']);
  }
}
