import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Buildings} from '../models/property-model/buildings';
import {HttpClient} from '@angular/common/http';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-building-details',
  templateUrl: './building-details.component.html',
  styleUrls: ['./building-details.component.css']
})
export class BuildingDetailsComponent implements OnInit {

  building: Buildings;
  isDataAvailable: boolean;

  constructor(public route: ActivatedRoute,
              public httpClient: HttpClient,
              private titleService: Title) {
    this.isDataAvailable = false;
  }

  async ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.httpClient.get('https://my-server.azurewebsites.net/api//get-property-by/' + id)
      .subscribe(
        (data: Buildings) => {
          if (data) {
            this.building = data;
            this.isDataAvailable = true;
            this.titleService.setTitle(this.building.title + ' - ' + environment.appName);
          } else {
            alert('Cannot load');
          }
        }
      );

  }
}



