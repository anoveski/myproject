import {RouterModule, Routes} from '@angular/router';
import {BuildingDetailsComponent} from './building-details.component';

export const buildingsDetailsRoutes: Routes = [
  {
    path: 'buildings/details/:id', component: BuildingDetailsComponent
  }
];
export const BuildingDetailsRouterModule = RouterModule.forChild(buildingsDetailsRoutes);
