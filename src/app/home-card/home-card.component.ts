import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PropertyCard} from '../models/property-card-model/property-card';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.css']
})
export class HomeCardComponent implements OnInit {
  @Input() building: PropertyCard;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  buildingDetails() {
    this.router.navigate(['buildings/details/', this.building.id]);
  }
}
