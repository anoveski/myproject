import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRouterModule} from './app.routes';
import {HomeComponent} from './home/home.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {
  MatButtonModule, MatCardModule, MatDatepickerModule,
  MatDialogModule, MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule, MatNativeDateModule, MatProgressSpinnerModule, MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {BuildingsComponent} from './buildings/buildings.component';
import {BuildingRouterModule} from './buildings/buildings.routes';
import {RegisterComponent} from './register/register.component';
import {RegisterRouterModule} from './register/register.routes';
import {LoginDialogComponent} from './login-dialog/login-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeCardComponent } from './home-card/home-card.component';
import { BuildingDetailsComponent } from './building-details/building-details.component';
import {BuildingDetailsRouterModule} from './building-details/building-details-routes';
import {AgmCoreModule} from '@agm/core';
import {NgxCarouselModule} from 'ngx-carousel';
import 'hammerjs';
import {Ng2CarouselamosModule} from 'ng2-carouselamos';
import {DatePipe} from '@angular/common';
import { HttpClientModule} from '@angular/common/http';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import {ProfilePageRoutes} from './profile-page/profile-page.routes';
import {AuthService} from './services/auth-service/auth.service';
import {AuthGuardService} from './services/auth-guard-service/auth-guard.service';
import { AddPropertyComponent } from './add-property/add-property.component';
import {AddPropertyRouterModule} from './add-property/add-property.routes';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import {UnauthorizedRouterModule} from './unauthorized/unauthorized.routes';
import {CKEditorModule} from 'ng2-ckeditor';
import { BuildingsCardComponent } from './buildings-card/buildings-card.component';
import { HousesComponent } from './houses/houses.component';
import {HousesRouterModule} from './houses/houses.routes';
import { LoadingDialogComponent } from './loading-dialog/loading-dialog.component';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    BuildingsComponent,
    RegisterComponent,
    LoginDialogComponent,
    HomeCardComponent,
    BuildingDetailsComponent,
    ProfilePageComponent,
    AddPropertyComponent,
    UnauthorizedComponent,
    BuildingsCardComponent,
    HousesComponent,
    LoadingDialogComponent
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    NgxCarouselModule,
    Ng2CarouselamosModule,
    CKEditorModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsAPIKey,
      libraries: ["places"]
    }),
    HttpClientModule,
    BuildingRouterModule,
    ReactiveFormsModule,
    FormsModule,
    MatProgressSpinnerModule,
    RegisterRouterModule,
    BuildingDetailsRouterModule,
    ProfilePageRoutes,
    HousesRouterModule,
    AddPropertyRouterModule,
    UnauthorizedRouterModule,
    AppRouterModule,
  ],
  entryComponents: [
    LoginDialogComponent,
    LoadingDialogComponent
  ],
  providers: [DatePipe, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
