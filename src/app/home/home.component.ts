import {Component, OnInit} from '@angular/core';
import {NgxCarousel} from 'ngx-carousel';

import {HttpClient} from '@angular/common/http';
import {PropertyCard} from '../models/property-card-model/property-card';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public buildings;
  public carouselOne: NgxCarousel;


  constructor(private httpClient: HttpClient,
              private titleService: Title) {
    this.titleService.setTitle(environment.appName);
  }

  ngOnInit() {
    this.carouselOne = {
      grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
      slide: 1,
      speed: 800,
      interval: 4000,
      point: {
        visible: false
      },
      load: 2,
      touch: true,
      loop: true,
      custom: 'banner'
    };

    this.httpClient.get('https://my-server.azurewebsites.net/api/get-all-properties')
      .subscribe(
        (data: PropertyCard) => {
          this.buildings = data;
        }
      )

  }


}
