import {RouterModule, Routes} from '@angular/router';
import {UnauthorizedComponent} from './unauthorized.component';

export const unauthorizedRoutes: Routes = [
  {
    path: 'unauthorized', component: UnauthorizedComponent
  }
];
export const UnauthorizedRouterModule = RouterModule.forChild(unauthorizedRoutes);
