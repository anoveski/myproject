import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginDialogComponent} from '../login-dialog/login-dialog.component';
import {MatDialog} from '@angular/material';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css'],
})
export class UnauthorizedComponent implements OnInit {

  private user_email: string;
  private password: string;
  public env = environment;

  constructor(private router: Router,
              private login: MatDialog,
              private titleService: Title) {
    this.titleService.setTitle('Advertise Property - ' + environment.appName);
  }

  ngOnInit() {
  }

  public loginDialog(): void {
    const dialogRef = this.login.open(LoginDialogComponent, {
      width: '400px',
      data: {user_email: this.user_email, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.user_email = result;
      this.password = result;
    });
  }

  public register(): void {
    this.router.navigate(['/register']);
  }
}
