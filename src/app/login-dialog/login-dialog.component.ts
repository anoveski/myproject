import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tokenNotExpired} from 'angular2-jwt';
import {AuthService} from '../services/auth-service/auth.service';
import {LoadingDialogComponent} from '../loading-dialog/loading-dialog.component';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {

  user_email: string;
  password: string;
  loadingDialog: any;
  env = environment;

  usernameFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(public dialogRef: MatDialogRef<LoginDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public router: Router,
              private loading: MatDialog,
              private auth: AuthService,
              private httpClient: HttpClient) {
  }

  ngOnInit() {
  }

  closeLogin(): void {
    this.dialogRef.close();
  }


  login() {
    this.loadingDialog = this.loading.open(LoadingDialogComponent, {
      width: '148px',
      height: '181px',
      data: { status: 'Logging in' }
    });
    let self = this;
    const loginCredentials = {
      user_email: this.user_email,
      password: this.password
    };

    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
    };

    this.httpClient.post('https://my-server.azurewebsites.net/api/login', loginCredentials, httpOptions)
      .subscribe(
        (data: any) => {
          if (data.success === false) {
            this.loadingDialog.close();
            alert(data.message);
          } else {
            this.loadingDialog.close();
            localStorage.setItem('token', data.token);
            self.auth.loggedIn();
            this.dialogRef.close();
            this.router.navigate(['add-property']);
          }
        }
      );
  }

  register() {
    this.router.navigate(['/register']);
    this.closeLogin();
  }
}
