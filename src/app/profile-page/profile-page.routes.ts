import {RouterModule, Routes} from '@angular/router';
import {ProfilePageComponent} from './profile-page.component';
import {AuthGuardService} from '../services/auth-guard-service/auth-guard.service';
import {UnauthorizedComponent} from '../unauthorized/unauthorized.component';

export const profilePageRoutes: Routes = [
  {
    path: 'my-profile', component: ProfilePageComponent, canActivate: [AuthGuardService]
  },
  {
    path: 'unauthorized', component: UnauthorizedComponent
  }
];
export const ProfilePageRoutes = RouterModule.forChild(profilePageRoutes);
