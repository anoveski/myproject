import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserInfo} from '../models/user-model/user-info';
import {Title} from '@angular/platform-browser';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})

export class ProfilePageComponent implements OnInit {

  id: string;
  first_name: string;
  last_name: string;
  gender: string;
  date_of_birth: string;
  phone_number: number;
  user_email: string;
  isDataAvailable: boolean;


  constructor(private httpClient: HttpClient,
              private titleService: Title) {
    this.titleService.setTitle('My Profile - ' + environment.appName);
    this.isDataAvailable = false;
  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})
    };
    this.httpClient.get('https://my-server.azurewebsites.net/api/user-info', httpOptions)
      .subscribe(
        (data: UserInfo) => {
            this.id = data.user.id;
            this.first_name = data.user.first_name;
            this.last_name = data.user.last_name;
            this.gender = data.user.user_gender;
            this.date_of_birth = data.user.date_of_birth;
            this.phone_number = data.user.phone_number;
            this.user_email = data.user.user_email;
           this.isDataAvailable = true;
        }
      )
  }

}
