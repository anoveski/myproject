import {RouterModule, Routes} from '@angular/router';
import {AddPropertyComponent} from './add-property.component';
import {AuthGuardService} from '../services/auth-guard-service/auth-guard.service';
import {UnauthorizedComponent} from '../unauthorized/unauthorized.component';

export const addPropertyRoutes: Routes = [
  {
    path: 'add-property', component: AddPropertyComponent, canActivate: [AuthGuardService]
  },
  {
    path: 'unauthorized', component: UnauthorizedComponent
  }
];
export const AddPropertyRouterModule = RouterModule.forChild(addPropertyRoutes);
