import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Marker} from '../models/marker-model/Marker';
import {DatePipe} from '@angular/common';
import {UserInfo} from '../models/user-model/user-info';
import {Router} from '@angular/router';
import {LoadingDialogComponent} from '../loading-dialog/loading-dialog.component';
import {MatDialog} from '@angular/material';
import {MapsAPILoader} from '@agm/core';
import {} from '@types/googlemaps';
import { Title } from '@angular/platform-browser';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-add-property',
  templateUrl: './add-property.component.html',
  styleUrls: ['./add-property.component.css']
})
export class AddPropertyComponent implements OnInit {

  buildingTypes = [
    {value: 'Multi-family house', viewValue: 'Multi-family house'},
    {value: 'Apartment', viewValue: 'Apartment'}
  ];
  yardStatuses = [
    {value: 'Yes', viewValue: 'Yes'},
    {value: 'No', viewValue: 'No'}
  ];

  today = new Date(Date.now());
  id: string;
  seller_first_name: string;
  seller_last_name: string;
  seller_phone_number: number;
  seller_email: string;

  markers: Marker[];

  rating: number;

  PropertyProfilePictureFile: File;
  PropertyForm: FormGroup;

  PropertyGalleryFile: File;

  loadingDialog: any;

  searchedLat: number;
  searchedLng: number;
  public zoom: number;
  public searchControl: FormControl;

  @ViewChild('search')
  public searchElementRef;


  @ViewChild('PropertyProfilePicture') Property_Profile_Picture;
  @ViewChild('PropertyGallery') Property_Gallery;


  constructor(private titleService: Title,
              private fb: FormBuilder,
              private datePipe: DatePipe,
              private router: Router,
              private loading: MatDialog,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private httpClient: HttpClient) {

    this.titleService.setTitle( 'Advertise Property - ' + environment.appName);
    this.rating = 0;

    this.setCurrentPosition();

    this.searchControl = new FormControl();

    this.PropertyForm = this.fb.group({
      'PropertyProfilePicture': ['', Validators.required],
      'PropertyGallery': ['', Validators.required],

      'Title': ['', Validators.required],
      'Price': ['', Validators.required],
      'TypeOfBuilding': ['', Validators.required],

      'YearOfConstruction': ['', Validators.required],
      'YardStatus': ['', Validators.required],

      'LotSize': ['', Validators.required],

      'YardSize': ['', Validators.required],

      'Details': ['', Validators.required],

      'NumberOfFloors': ['', Validators.required],
      'NumberOfBathrooms': ['', Validators.required],
      'NumberOfToilets': ['', Validators.required],
      'NumberOfKitchens': ['', Validators.required],
      'NumberOfRooms': ['', Validators.required],
      'NumberCorridors': ['', Validators.required],

      'BathroomsRating': ['', Validators.required],
      'ToiletsRating': ['', Validators.required],
      'KitchenRating': ['', Validators.required],
      'RoomsRating': ['', Validators.required],
      'CorridorsRating': ['', Validators.required],
      'RoofRating': ['', Validators.required],
      'WindowsRating': ['', Validators.required],


      'AddressLine': ['', Validators.required],
      'Place': ['', Validators.required],
      'ZipCode': ['', Validators.required],
      'Province': ['', Validators.required],
      'Country': ['', Validators.required],
      'CountryIsoCode': ['', Validators.required]

    })


  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})
    };
    this.httpClient.get('https://my-server.azurewebsites.net/api/user-info', httpOptions)
      .subscribe(
        (data: UserInfo) => {
          this.id = data.user.id;
          this.seller_first_name = data.user.first_name;
          this.seller_last_name = data.user.last_name;
          this.seller_phone_number = data.user.phone_number;
          this.seller_email = data.user.user_email;
        }
      );

    // set google maps defaults
    this.zoom = 4;
    this.searchedLat = 39.8282;
    this.searchedLng = -98.5795;

    // create search FormControl
    this.searchControl = new FormControl();

    // set current position
    this.setCurrentPosition();

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const nativeHomeInputBox = <HTMLInputElement>document.getElementById('searchedAddress');
      const autoComplete = new google.maps.places.Autocomplete(nativeHomeInputBox, {
        types: ['address']
      });
      autoComplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autoComplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.searchedLat = place.geometry.location.lat();
          this.searchedLng = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });

  }

  removeMarker(marker) {
    for (let i = 0; i < this.markers.length; i++) {
      if (marker.lat === this.markers[i].lat && marker.lng === this.markers[i].lng) {
        this.markers.splice(i, 1);
        localStorage.removeItem('marker');
      }
    }
  }

  totalRating(value) {
    const nonRoundedRating = (value.BathroomsRating + value.ToiletsRating +
      value.KitchenRating + value.RoomsRating +
      value.CorridorsRating + value.RoofRating + value.WindowsRating) / 7;
    this.rating = Math.round(nonRoundedRating);
  }

  addProperty(value) {

    this.loadingDialog = this.loading.open(LoadingDialogComponent, {
      width: '154px',
      height: '202px',
      data: {status: 'Advertising property'}
    });

    const PropProfilePhoto = this.Property_Profile_Picture.nativeElement;
    if (PropProfilePhoto.files && PropProfilePhoto.files[0]) {
      this.PropertyProfilePictureFile = PropProfilePhoto.files[0];
    }
    const PropImageFile: File = this.PropertyProfilePictureFile;

    const PropGallery = this.Property_Gallery.nativeElement;
    this.PropertyGalleryFile = PropGallery.files;
    const PropGalleryFile: File = this.PropertyGalleryFile;


    const formData: FormData = new FormData();
    formData.append('title', value.Title);
    formData.append('price', value.Price.toString());
    formData.append('rating', this.rating.toString());
    formData.append('sellerId', this.id);
    formData.append('sellerName', this.seller_first_name);
    formData.append('sellerSurname', this.seller_last_name);
    formData.append('phoneNumber', this.seller_phone_number.toString());
    formData.append('seller_email', this.seller_email);
    formData.append('advertisedDate', this.datePipe.transform(this.today, 'MM/dd/yyyy'));
    formData.append('typeOfBuilding', value.TypeOfBuilding);
    formData.append('yearOfConstruction', value.YearOfConstruction.toString());
    formData.append('baseSize', value.LotSize.toString());
    formData.append('yardStatus', value.YardStatus);
    formData.append('yardSize', value.YardSize.toString());
    formData.append('numberOfFloors', value.NumberOfFloors.toString());
    formData.append('numberOfBathrooms', value.NumberOfBathrooms.toString());
    formData.append('numberOfToilets', value.NumberOfToilets.toString());
    formData.append('numberOfKitchens', value.NumberOfKitchens.toString());
    formData.append('numberOfRooms', value.NumberOfRooms.toString());
    formData.append('numberCorridors', value.NumberCorridors.toString());
    formData.append('addressLine', value.AddressLine);
    formData.append('zipCode', value.ZipCode.toString());
    formData.append('place', value.Place);
    formData.append('province', value.Province);
    formData.append('country', value.Country);
    formData.append('countryIsoCode', value.CountryIsoCode);
    formData.append('latitude', JSON.parse(localStorage.getItem('marker')).lat);
    formData.append('longitude', JSON.parse(localStorage.getItem('marker')).lng);
    formData.append('details', value.Details);
    formData.append('buildingProfilePicture', PropImageFile, PropImageFile.name);
    for (let i = 0; i < PropGallery.files.length; i++) {
      formData.append('buildingGallery', PropGalleryFile[i], PropGalleryFile[i].name);
    }
    formData.append('bathroomsRating', value.BathroomsRating.toString());
    formData.append('toiletsRating', value.ToiletsRating.toString());
    formData.append('kitchenRating', value.KitchenRating.toString());
    formData.append('roomsRating', value.RoomsRating.toString()); //
    formData.append('corridorsRating', value.CorridorsRating.toString());
    formData.append('roofRating', value.RoofRating.toString());
    formData.append('windowsRating', value.WindowsRating.toString());

    this.httpClient.post('https://my-server.azurewebsites.net/api/add-building', formData)
      .subscribe(
        (data: any) => {
          if (data.success === false) {
            this.loadingDialog.close();
            alert(data.message);
          } else {
            this.loadingDialog.close();
            localStorage.removeItem('marker');
            alert(data.message);
            this.router.navigate(['home']);
          }
        }
      );
  }

  setPropertyLocation($event: any) {
    localStorage.setItem('marker', JSON.stringify($event.coords));

    if (JSON.parse(localStorage.getItem('marker')) != null) {
      this.markers = [JSON.parse(localStorage.getItem('marker'))];
    } else {
      this.markers = [];
    }

  }

  markerDragEnd(m: any, $event) {
    localStorage.setItem('marker', JSON.stringify($event.coords));
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.searchedLat = position.coords.latitude;
        this.searchedLng = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }
}
