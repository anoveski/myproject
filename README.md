# README #

My project will be about application for buying and selling buildings (houses, buildings and other).

The application will have data about the building like:

      •	pictures – outside and inside of almost every part like the corridors, rooms, kitchen, toilets;

      •	location – on map and the address;

      •	rating – based on location, state of the parts corridors, kitchen, rooms, roof, how old is the building;

      •	details – other details about the building depending on the seller;

      •	price – the price will be seller’s choice, but there will be option if the seller wants a suggestion about the price,
                based on the rating of the building.

The customers, sellers and buyers will have different permissions in the application.

      •	The sellers – will have to login to add buildings to sell;

      •	The buyers – will be able to browse the buildings, but if they want to write to the seller or to schedule a meeting
                     with the seller at the building they will have to login on the application. 


Resources of the application:

	       All the data will be saved on a backend server.
	
	       The server will handle the permissions for each user (seller or buyer) by the login.

Development frameworks:

	       Angular 2 - for the front end of the application
	
	       NodeJS or myPHP – for the backend server of the application 
